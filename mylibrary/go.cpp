#include "go.h"
#include "randomai.h"
#include "list"


namespace mylib {
namespace go {

Block::Block(std::shared_ptr<Stone> stone)
{
  _color = stone->_color;
  _stone.insert((*stone)._point);
  std::vector<Point> b_points;

  //StoneColor b=stone->_board->stone(stone->north());

  if (stone->has_north() && (stone->_board->stone(stone->north()))==stone->_color)
    {
       addBlock(std::shared_ptr<Block>(this));
        b_points.push_back(stone->north());

    }
    if(stone->has_south() && (stone->_board->stone(stone->south()))==stone->_color)
    {
        addBlock(std::shared_ptr<Block>(this));
        b_points.push_back(stone->south());
    }
    if(stone->has_east() && (stone->_board->stone(stone->east()))==stone->_color)
    {
        addBlock(std::shared_ptr<Block>(this));
        b_points.push_back(stone->east());
    }
    if(stone->has_west() && (stone->_board->stone(stone->west()))==stone->_color)
    {
        addBlock(std::shared_ptr<Block>(this));
        b_points.push_back(stone->west());
    }
    _boundary.insert(Boundary(b_points));
  }

  void Block::joinBlock(std::shared_ptr<Block> newBlock, std::shared_ptr<Block> existingblock)
  {
      if (newBlock==existingblock)
          return ;
      newBlock-> addBlock(existingblock);
      _block.remove(existingblock);
  }
std::set<Point> Block::getpoint() const
  {
      return this->_stone;
  }

  void Block::addpoint(Point intersection)
  {
    _stone.insert(intersection);
  }

  std::shared_ptr<Block> Block::findBlock(Point intersection) const
  {
      for(std::shared_ptr<Block> block: _block)
      {
          for(auto point:block-> getpoint())
          {
              if(point==intersection)
                  return block;
          }
      }
      return nullptr;
  }

  void Block::addBlock(std::shared_ptr<Block> block)
  {
      _block.push_back(block);
  }












  namespace priv {
  Stone_base::Stone_base(Point _point, StoneColor _color, std::shared_ptr<Board> _board)
   {
         this->_point=_point;
         this->_color=_color;
         this->_board=_board;
   }
  bool Stone_base::has_north() const{
  return(*(this->_board)).hasStone(Point(_point.first+1,_point.second));
  }

  bool Stone_base::has_west() const{
  return(*(this->_board)).hasStone(Point(_point.first-1,_point.second));

}
  bool Stone_base::has_east() const{
  return(*(this->_board)).hasStone(Point(_point.first,_point.second+1));

  }

  bool Stone_base::has_south()const{
  return(*(this->_board)).hasStone(Point(_point.first,_point.second-1));

}
  Point Stone_base::north() const
    {
    return Point(_point.first,_point.second-1);
    }
  Point Stone_base::south() const
    {
    return Point(_point.first,_point.second+1);
    }
  Point Stone_base::east() const
    {
    return Point(_point.first+1,_point.second);
    }
  Point Stone_base::west() const
    {
    return Point(_point.first-1,_point.second);
    }




    Board_base::Board_base( Size size ) {

      resetBoard(size);
    }

    Board_base::Board_base(Board::BoardData&& data, StoneColor turn, bool was_previous_pass)
      : _current{std::forward<Board::BoardData>(data),turn,was_previous_pass}
    {
      // ... init ...
    }

    Board_base::Position::Position(Board::BoardData&& data, StoneColor trn, bool prev_pass)
      : board{data}, turn{trn}, was_previous_pass{prev_pass} {}


    void
    Board_base::resetBoard(Size size) {

      _current.board.clear();
      _size = size;
      _current.turn = StoneColor::Black;
    }

    Size
    Board_base::size() const {

      return _size;
    }

    bool
    Board_base::wasPreviousPass() const {

      return _current.was_previous_pass;
    }

    StoneColor
    Board_base::turn() const {

      return _current.turn == StoneColor::Black ? StoneColor::White : StoneColor::Black;
    }

  }

  void
  Board::placeStone(Point intersection) {



    _current.board[intersection] = turn();
    _current.turn = turn();
  }

  void
  Board::passTurn() {

    _current.turn = turn();
  }

  bool
  Board::hasStone(Point intersection) const {

    return _current.board.count(intersection);
  }

  StoneColor
  Board::stone(Point intersection) const {

    return _current.board.at(intersection);
  }


  bool
  Board::isNextPositionValid(Point /*intersection*/) const {

    return true;
  }

  Engine::Engine()
    : _board{}, _game_mode{}, _active_game{false},
      _white_player{nullptr}, _black_player{nullptr} {}

  void
  Engine::newGame(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::VsAi;
    _active_game = true;

    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),   StoneColor::White);
  }

  void
  Engine::newGameAiVsAi(Size size) {

    _board.resetBoard(size);

    _game_mode = GameMode::Ai;
    _active_game = true;

    _white_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<RandomAi>(shared_from_this(),StoneColor::White);
  }

  void
  Engine::newGameFromState(Board::BoardData&& board, StoneColor turn, bool was_previous_pass) {

    _board = Board {std::forward<Board::BoardData>(board),turn,was_previous_pass};

    _game_mode = GameMode::VsPlayer;
    _active_game = true;
    _white_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::Black);
    _black_player = std::make_shared<HumanPlayer>(shared_from_this(),StoneColor::White);
  }

  const Board&
  Engine::board() const {

    return _board;
  }

  const GameMode&
  Engine::gameMode() const {

    return _game_mode;
  }

  StoneColor
  Engine::turn() const {

    return _board.turn();
  }

  const std::shared_ptr<Player>
  Engine::currentPlayer() const {

    if(      turn() == StoneColor::Black ) return _black_player;
    else if( turn() == StoneColor::White ) return _white_player;
    else                              return nullptr;
  }

  void
  Engine::placeStone(Point intersection) {

    _board.placeStone(intersection);
  }

  void
  Engine::passTurn() {

    if(board().wasPreviousPass()) {
      _active_game = false;
      return;
    }

    _board.passTurn();
  }

  void
  Engine::nextTurn(std::chrono::duration<int,std::milli> think_time) {

    if( currentPlayer()->type() != PlayerType::Ai) return;

    auto p = std::static_pointer_cast<AiPlayer>(currentPlayer());

    p->think( think_time );
    if( p->nextMove() == AiPlayer::Move::PlaceStone )
      placeStone( p->nextStone() );
    else
      passTurn();
  }

  bool
  Engine::isGameActive() const {

    return _active_game;
  }

  bool
  Engine::validateStone(Point pos) const {

    return _board.isNextPositionValid(pos);
  }

  Boundary::Boundary(std::vector<Point>points)  : _points{points}
  {


 }

} // END namespace go
} // END namespace mylib
