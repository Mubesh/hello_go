
// My Go Engine
#include "../../mylibrary/go.h"

// gtest
#include <gtest/gtest.h>


namespace go = mylib::go;


TEST(GoEngineTest_Board,FromStateConstructor) {

//      |x x x x x|
//      |x x x x x|
//      |x x b x x|
//      |x x x x x|
//      |x x x x x|

  go::Board::BoardData board_data;
  board_data[go::Point(3,3)] = go::StoneColor::Black;

  // Create a engine from state
  go::Board board {std::move(board_data),go::StoneColor::White,false};


  // EXPECT a stone at (3,3)
  EXPECT_TRUE(board.hasStone({3,3}));

  // EXPECT a black stone at (3,3)
  EXPECT_TRUE(board.stone({3,3}) == go::StoneColor::Black);
}

TEST(GoEngineTest_LegalStonePlacements,SimpleKoRuleTest) {

//      |x x x x x|
//      |x x w x x|
//      |x w b w x|
//      |x b x b x|
//      |x x b x x|

  go::StoneColor B = go::StoneColor::Black;
  go::StoneColor W = go::StoneColor::White;

  go::Board::BoardData b;
                b[{3,2}] = W;
  b[{2,3}] = W; b[{3,3}] = B; b[{4,3}] = W;
  b[{2,4}] = B;               b[{4,4}] = B;
                b[{3,5}] = B;


  // Create a PvP engine
  auto engine = std::make_shared<go::Engine>();
  engine->newGameFromState( std::move(b), go::StoneColor::White, false );

  // Set up the game for the 'ko' rule to trigger:
  // white removes the freedoms of black's Stone on (3,3)
  // and captures it.
  engine->placeStone(go::Point(3,4));

  // Play a illegal move for black triggering the 'ko' rule
  // when black tries to capture whites (3,4).
  EXPECT_FALSE(engine->validateStone(go::Point(3,4)));
}

TEST(StoneTest, Test_South) {

    go::Stone newStone = go::Stone(go::Point{2,2}, go::StoneColor::Black, nullptr);

    go::Point south = go::Point(1,2);

    EXPECT_TRUE(south == newStone.south());
    }
